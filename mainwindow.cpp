#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTimer>
#include <random>
#include <QTextEdit>
#include <ctime>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    amountOfJobs=0;//initialize amountOfJobs to 0
    inisBValue=0;
    amountOfJobsBQ=0;
    amountOfPartitions=0;
    cpuIsActive=false;
    ui->setupUi(this);
    ui->sB_Jobs->setValue(1);
    ui->tb_RAM->setEditTriggers(QAbstractItemView::NoEditTriggers); //Disable editing TEST
    ui->tE_Status->setText("Total Jobs: "+QString::number(amountOfJobs));
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(on_pB_Start_clicked()));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_sB_Jobs_valueChanged(int value)
{
    inisBValue=value;
}

void MainWindow::on_pB_New_clicked()
{

    ui->tE_Status->clear();   //Clear text editor on click
    mainMemory.clear(); //clear vector on start
    amountOfJobs+=inisBValue;
    ui->sB_Jobs->setValue(1); //Reset spinbox counter to 1 on click
    ui->tb_RAM->setEnabled(true);
    //On click generate random Job ID numbers based on the spinner amount
    std::default_random_engine seed(time(nullptr));
    std::uniform_int_distribution<int> iJobID(0,99);
    std::uniform_int_distribution<int> iJobRank(20,99);
    std::uniform_int_distribution<int> iRMemSize(1,700);
    std::uniform_int_distribution<int> iNumPartitions(5,7);
    std::uniform_int_distribution<int> iPartSize(600,1200);

    //Generate rand. partition size value
    numPartitions = iNumPartitions(seed);
    remainingPartitions = numPartitions;

    //Create vector of partitions based of numPartions value and update rows of table
    ui->tb_RAM->setRowCount(numPartitions);
    ui->tE_Status2->clear();
    ui->tE_Status2->append("RAM consists of "+QString::number(numPartitions)+" partitions");

    for (int i=0; i<numPartitions; i++)
    {
       // ui->tE_Status2->append("Elements in vec. mainMemory: "+QString::number(mainMemory.size())); //TEST
        PartitionStatus RAM(i,iPartSize(seed),false);
        mainMemory.push_back(RAM);
        ui->tb_RAM->setItem(i,0, new QTableWidgetItem(QString::number(mainMemory[i].getLocation())));
        ui->tb_RAM->setItem(i,1, new QTableWidgetItem(QString::number(mainMemory[i].getPartSize())+"K"));
        if (mainMemory[i].getPartUsage()==0){ui->tb_RAM->setItem(i,4, new QTableWidgetItem("Free"));} //Check if part. is free or in use
        else {ui->tb_RAM->setItem(i,4, new QTableWidgetItem("In Use"));}
    }


    ui->tE_Status->append("Remaining Jobs: "+QString::number(amountOfJobs));

    if (amountOfJobs == 0){ui->tE_Status->append("No Jobs created");}
    else
    {
        //Pass in Jobs objects into ready queue
        if (!readyQueue.empty())
        {
            //Clear readyQueue
            unsigned rQSize = readyQueue.size();
            for (unsigned int i=0; i<rQSize; i++){readyQueue.pop();}

            //Fill readyQueue with Job objects
            for (int i=0; i<amountOfJobs; i++){
                readyQueue.emplace(iJobID(seed),iJobRank(seed),iRMemSize(seed));
            }
        }
        else
        {
            //Fill readyQueue with Job objects
            for (int i=0; i<amountOfJobs; i++){readyQueue.emplace(iJobID(seed),
                                                                  iJobRank(seed),
                                                                  iRMemSize(seed));}
        }

        //Display contents of Ready Queue
        dReadyQueue=readyQueue; //Copy readyQueue contents into dReadyQueue
        //Update next in Queue label
        Jobs a;
        a=readyQueue.top();

        //Display Jobs in Queue
        for (int i = 0; i<amountOfJobs;i++){
            a = dReadyQueue.top();
            ui->tE_Status->append("Job#"+QString::number(i+1)+"\n"
                                  "Job ID: "+QString::number(a.getID())+"\n"+
                                  "Job Rank: "+QString::number(a.getRank())+"\n"+
                                  "Job Mem. Size: "+QString::number(a.getRMemSize())+"K"+"\n");
            dReadyQueue.pop();
        }

        ui->pB_Start->setEnabled(true); //Enable start button
        dReadyQueue = readyQueue;
        dReadyQueue.pop();  //Pop to show next job in queue
        ui->tE_Status->append("--------------------------\n");
    }

}

void MainWindow::on_pB_Start_clicked()
{
    Jobs a,b;
    int freeSpace=numPartitions;
    bool toggleJobRNG = false;
    cpuIsActive = true;
    timer->start(TIMEVAL); //TIMEVAL= 6 seconds

    if (!dReadyQueue.empty())       //Pop contents out of dReadyQueue
    {
        b = dReadyQueue.top();
        dReadyQueue.pop();
    }

    if (!readyQueue.empty())        //Pop contents out of readyQueue
    {
        if (toggleJobRNG == true)//TESTING
        {
            //Randomly choose a partition to deallocate
            std::default_random_engine seed(time(nullptr));
            std::uniform_int_distribution<int> iEndJob(0,mAllocation.size());
            int r=iEndJob(seed);
            mainMemory[r].setPartUsage(false); //Free
            ui->tE_Status2->append("Job ID "+QString::number(mAllocation[r].getJobID())+" has been deallocated\n"
                                                             "Partition location "+QString::number(mAllocation[r].getAddress())+
                                                             " is now free\n");
        }
        ui->pB_Terminate->setEnabled(true);
        a = readyQueue.top();
        ui->tE_Status2->append("Remaining Jobs: "+QString::number(amountOfJobs));
        ui->tE_Status2->append("JobID "+QString::number(a.getID())+" being processed\n");

        //Allocate memory: Fill the partitions with Jobs
        for (int i=0; i<numPartitions; i++)
        {
            if (a.getRMemSize() <= mainMemory[i].getPartSize())
            {
                if (mainMemory[i].getPartUsage()==false)
                {
                    //Allocate memory: Fill the partitions with Jobs
                    mainMemory[i].setPartUsage(true);
                    MemAllocation allocate(mainMemory[i].getLocation(), mainMemory[i].getPartSize(), mainMemory[i].getPartUsage(),
                                           a.getID(), a.getRMemSize(), (mainMemory[i].getPartSize()-a.getRMemSize()));
                    mAllocation.push_back(allocate);

                    //Display mAllocation contents
                    ui->tb_RAM->setItem(i,0, new QTableWidgetItem(QString::number(allocate.getAddress())));
                    ui->tb_RAM->setItem(i,1, new QTableWidgetItem(QString::number(allocate.getPartSize())+"K"));
                    ui->tb_RAM->setItem(i,2, new QTableWidgetItem(QString::number(allocate.getJobID())));
                    ui->tb_RAM->setItem(i,3, new QTableWidgetItem(QString::number(allocate.getMemSize())+"K"));
                    ui->tb_RAM->setItem(i,5, new QTableWidgetItem(QString::number(allocate.getPartSize())+"K"));
                    if (mainMemory[i].getPartUsage()==0){ui->tb_RAM->setItem(i,4, new QTableWidgetItem("Free"));} //Check if part. is free or in use
                    else {ui->tb_RAM->setItem(i,4, new QTableWidgetItem("In Use"));}
                    ui->tE_Status2->append("Job ID "+QString::number(allocate.getJobID())+" allocated "+
                                           QString::number(mainMemory[i].getPartSize())+"K of space at location "+
                                           QString::number(allocate.getAddress())+"\n");
                    amountOfJobs--;
                    freeSpace--;
                    //ui->tE_Status2->append("Free Space Left: "+QString::number(freeSpace)); //Test
                    counter++; //Used to check if job searched through all partitions
                    readyQueue.pop();
                    break;
                }
                else if (freeSpace>1) //Testing
                {
                    int spaceCount =0;
                    int tmpLocation;
                    //Find location of free space(s), add up memory, and merge partitions
                    for (int j=0; j<numPartitions; j++){
                        if (mainMemory[j].getPartUsage() == false){
                            spaceCount++;
                            if (spaceCount==1){tmpLocation;}
                        }
                    }
                    //If spaceCount is 1, display error then deallocate a space in use.
                    if (spaceCount==1)
                    {
                        ui->tE_Status2->append("Error: JobID "+QString::number(a.getID())+" cannot allocate a space\n");
                        //Randomly choose a partition to deallocate
                        std::default_random_engine seed(time(nullptr));
                        //std::uniform_int_distribution<int> iEndJob(0,mAllocation.size());
                        std::uniform_int_distribution<int> iEndJob(0,numPartitions);

                        int r=iEndJob(seed);
                        mainMemory[r].setPartUsage(false); //Free
                        ui->tE_Status2->append("Job ID "+QString::number(mAllocation[r].getJobID())+" has been deallocated\n"
                                                                         "Partition location "+QString::number(mAllocation[r].getAddress())+
                                                                         " is now free\n");
                        if (mainMemory[r].getPartUsage()==0){ui->tb_RAM->setItem(r,4, new QTableWidgetItem("Free"));} //Check if part. is free or in use
                        else {ui->tb_RAM->setItem(r,4, new QTableWidgetItem("In Use"));}
                    }
                }//End Testing
            }
        }
    }
    else
    {
        timer->stop();
        ui->tE_Status2->append("\nAll Jobs Processed!");
        ui->pB_Start->setEnabled(false);
        ui->pB_Terminate->setEnabled(false);
    }
}

void MainWindow::on_pB_Terminate_clicked()
{
    Jobs a;

    //If a Job is being processed (check with bool. condition) time slice and move to blocked queue
    if (amountOfJobs == 0){ui->tE_Status2->append("\nOnly 1 Job is being processed\nCannot Terminate");}
    else if(cpuIsActive && (timer->remainingTime()>0))
    {
        amountOfJobsBQ++;
        a = readyQueue.top();
        waitQueue.emplace(a.getID(),a.getRank(),a.getRMemSize());
        ui->tE_Status2->append("\nJobID "+QString::number(a.getID())+" Terminated");

        readyQueue.pop();
        amountOfJobs--;
        timer->stop();
        ui->tE_Status2->append("\nProcess Interupted!\nPress Start to continue...\n");
        ui->pB_Terminate->setEnabled(false);
    }
}
