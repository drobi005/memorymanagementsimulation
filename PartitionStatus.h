#include <iostream>

class PartitionStatus
{
public:
    PartitionStatus(){}
    PartitionStatus(int jLocation,int jPartSize,bool jPartUsage){location=jLocation; partSize=jPartSize; partUsage=jPartUsage;}
    ~PartitionStatus(){}
    int   getLocation(){return location;}
    int   getPartSize(){return partSize;}
    bool  getPartUsage(){return partUsage;}
    void  setLocation(int a){location=a;}
    void  setPartSize(int a){partSize=a;}
    void  setPartUsage(bool a){partUsage=a;}
private:
    int  location;
	int  partSize;
    bool partUsage;
};
