#include <iostream>

class MemAllocation
{
public:
    MemAllocation(){}
    MemAllocation(int jAddress,int jPartSize,bool jPartUsage, int jJobID, int jJobMemSize, int jFragmentation)
    {address=jAddress; partSize=jPartSize; partUsage=jPartUsage;
     jobID=jJobID; jobMemSize=jJobMemSize; fragmentation=jFragmentation;}
    ~MemAllocation(){}
    int   getAddress(){return address;}
    int   getPartSize(){return partSize;}
    bool  getPartUsage(){return partUsage;}
	int   getJobID(){return jobID;}
    int   getMemSize(){return jobMemSize;}
    int   getFragmentation(){return fragmentation;}
    void  setAddress(int a){address=a;}
    void  setPartSize(int a){partSize=a;}
    void  setPartUsage(bool a){partUsage=a;}
    void  setJobID(int a){jobID=a;}
    void  setMemSize(int a){jobMemSize=a;}
    void  setFragmentation(int a){fragmentation=a;}
private:
    int  address;
	int  partSize;
    bool partUsage;
	int  jobID;
	int  jobMemSize;
    int  fragmentation; //fragmentation
};
