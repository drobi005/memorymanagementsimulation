#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <queue>
#include <vector>
#include <QTimer>
#include <QThread>
#include "Jobs.h"
#include "MemAllocation.h"
#include "PartitionStatus.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    int amountOfJobs,inisBValue,amountOfJobsBQ,amountOfPartitions;
    bool cpuIsActive;
    const int TIMEVAL = 6000; //6 seconds
    int rTime = 3;
    int counter=0;
    int numPartitions;
    int remainingPartitions;
    std::priority_queue<Jobs> readyQueue;
    std::priority_queue<Jobs> waitQueue;
    std::priority_queue<Jobs> dReadyQueue;
    std::priority_queue<Jobs> dBlockQueue;
    std::vector<PartitionStatus> mainMemory;
    std::vector<MemAllocation> mAllocation;

private slots:

    void on_sB_Jobs_valueChanged(int value);
    void on_pB_New_clicked();
    void on_pB_Start_clicked();
    void on_pB_Terminate_clicked();

private:
    Ui::MainWindow *ui;
    QTimer *timer;
};

#endif // MAINWINDOW_H
