#include <iostream>

class Jobs
{
public:
    Jobs(){}
    Jobs(int jID,int jRank,int jRMemSize){id=jID;rank=jRank;rMemSize=jRMemSize;}
    ~Jobs(){}
    int getID(){return id;}
    int getRank(){return rank;}	//rank=priority
    int getRMemSize(){return rMemSize;}
    void setID(int a){id=a;}
    void setRank(int a){rank=a;}
    void setRMemSize(int a){rMemSize=a;}

    bool operator< (const Jobs& otherJobs)const
    {
        if (rank<otherJobs.rank){return true;}
        else {return false;}
    }
private:
    int id;
    int rank;
    int rMemSize; //requested memory size
};
